// import { message } from './exporter'
//const and let example within scope and out of scope

const hellofunc=(val1)=>{
  // const val1=5

  // const val2=6
  // const sample=()=>{
  //   console.log(`Calling inner val2 -:${val2}`)
  // }
  // sample()
  // console.log('val2 ::'+val2)
}
console.log('hello func '+hellofunc(6))
// console.log(val2);
// console.log(`val1: ${val1}`);

const extra=5
console.log(`extra :${extra}`);
  // extra=5;
const obj={
    attr1:1,
    attr2:'text'
}
console.log(obj.attr1);
obj.attr1=2;
console.log(obj.attr1);


//arrow function
const printName=name=>{
  var trail=1111;
    return `Name is : ${name}`;
}
// console.log(`trail  is : ${trail}`)
console.log(printName('ABCD'));


//Object ane Array Destructing
let {attr1,attr2}=obj
console.log(`attr1:${attr1}`);
console.log(`attr2:${attr2}`);
  //array destructing 
const arr=['some','values','of','s_array'];
// const allArra= arr.map(num=>{ 
//   return num.toLocaleUpperCase();
// })
const allArra= arr.map(num=>{ 
  return `${num.startsWith('s') }`;
})
console.log('allArra :'+allArra)//map with array string
let[one,two,three,four]=arr;
console.log(one);
console.log(two);
console.log(three);
console.log(four);


///Default parameter
const sumFunc=(a,b=1)=>{
  return a+b;
}
console.log(sumFunc(5,1))
console.log(sumFunc(5))
//


const numArr=
[
  {one:1,two:12,three:23,four:3},
  {one:2,two:22,three:33,four:4}
];

console.log(`numArr :${numArr}`)
// const allArr= numArr.filter(num=>{ 
//   return num.three >= 30;
// })
const allArr= numArr.map(num=>{ 
  return num.three * 2;
})
console.log('some map opertion :'+ allArr)
//
// const numArr=[1,12,23]
console.log(`NumArr: ${numArr}`)
const findFunc=(value,index)=>{
  console.log(value)
  return value<8;
}
console.log('length :'+numArr.one)
// var first=numArr.find(findFunc);
var firstindex=numArr.findIndex(findFunc);
console.log(`firstindex : ${firstindex}`);
// console.log(`First: ${first}`);

const x=0,y=0
// objs = {x,y};
let objs={
  some1:'text',
  ['some'+hellofunc()]:42
}
console.log(objs) 

const person = {
  firstName: 'Tom',
  lastName: 'Cruise',
  actor: true,
  age: 54,
}
const {firstName, name, age} = person
console.log(firstName)


var list = [ 1, 2, 3 ]
var [ a, , b ] = list
// [ b, a ] = [ a, b ]
console.log(list)
console.log([ b, a ] = [ b ,a])
console.log([a])
var list = [ 7, 42 ]
var [ a = 1, b = 2, c = 3, d ] = list
console.log(a,b,c)
// console.log(message)
//spread operator
const aa=[1,2]
const bb=[3]
const cc=[4]
const dd=[5]
const e=[...aa,...bb,...cc,...dd]
console.log(e)
//
const songs=[
  {id:1,name:'name1',artist:'artist1'},
  {id:2,name:'name2',artist:'artist2'},
  {id:3,name:'name3',artist:'artist3'}
];
console.log(songs);
const someIds = songs.filter(song => {
  // console.log(song.artist);
  return song.id >= 2;
});
console.log(someIds);

//built in methods
console.log('qa'.repeat(4))
console.log('foo'.repeat(3))
console.log(Math.trunc(42.007))
//
function g ({ name: n, val: v }) {
  console.log(n, v)
}
  console.log('hello','bye')
  ///
  var dest={quux:0}
  var src1={foo:1,bar:2}
  var src2={foo:3,bar:4}
  Object.assign(dest,src1,src2)
  console.log('Object Assign : ' ,`${Object.assign(dest,src1,src2)}`)
///////////////////////////////////
let cleanRoom=async()=>{
  return new Promise(function(resolve,reject){
    // resolve(" Cleane ");
    setTimeout(() => resolve(" Cleane "), 3000);
  });
};
let removeGarbage=function(message){
  return new Promise(function(resolve,reject){
    // resolve(message+" Removed garbage ");
    setTimeout(() => resolve(message+ " Removed Garbage "), 2000);
  });
};
let winIcecream=function(message){
  return new Promise(function(resolve,reject){
    // resolve(message+" donee ");
    setTimeout(() => resolve(message+" doneee "), 2000);
    
  });
};
cleanRoom().then(function(result){
  return removeGarbage(result)
}).then(function(result){
  return winIcecream(result);
}).then(function(result){
  console.log(' finished ' + result);
})
// Promise.all([cleanRoom(),removeGarbage(),winIcecream()]).then(function(){
//   console.log('all completed')
// });
////
/*
console.log('person1 goes');
console.log('person2 goes');

const preMovie = async () => {

  const person3PromiseTogoAfter = new Promise((resolve, reject) => {
    setTimeout(() => resolve('Finish'), 3000);
  });
  const getRunner =  new Promise((resolve, reject) => {
		setTimeout(() => resolve('running'), 2000);
  });
  
  const addFast =  new Promise((resolve, reject) => {
		setTimeout(() => resolve('Fast'), 3000);
  });

  let ticket = await person3PromiseTogoAfter;

  console.log(`got the ${ticket}`);
  
  
  let runner = await getRunner;
  console.log(`X-person: is ${runner}`);
	
  let fast = await addFast;
  console.log(`added ${fast} mode`);
  
  return ticket;
};

preMovie().then((t) => console.log(`X-person will ${t}`));

console.log('person4 goes');
*/